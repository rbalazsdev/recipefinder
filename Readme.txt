ReadMe

Recipe finder
Given a list of items in the fridge (presented as a csv list), and a collection of recipes (a collection of JSON formatted recipes), produce a recommendation for what to cook tonight. 



Solution chosen to determine the recipe with the closest due date ingredient is based on:

- finding all recipes that have all the ingredients available.
- filtering the ingredients after due date 
- go trough the sorted list of ingredients and lookup in found recipes for the ingredient. The first ingredient found is directly linked with the “winning” recipe.


TO DOs:

- improve the search in the recipes. For example: when iterating trough recipes  store also the date of the ingredients associated with that recipe. Create a list of recipes and filter it after the date.
- for reusability, create a Kitchen class that handles all the recipe finding, and keep the view controller only for I/O. 
- load the CSV file or JSON file from given URL or open from email attachment.
 -add more unit tests




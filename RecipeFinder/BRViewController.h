//
//  BRViewController.h
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRViewController : UIViewController <UITextFieldDelegate>

@end

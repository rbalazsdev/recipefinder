//
//  BRRecipesHandler.m
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRRecipesHandler.h"
#import "BRRecipe.h"
#import "BRIngredient.h"

@implementation BRRecipesHandler

- (void)loadRecipesFormFilePath:(NSString*)filePath{

    NSError *error;
    NSString *contentJSON =  [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    if (contentJSON == nil) {
        NSLog (@"Error loading JSON file %@", error);
        self.recipes = nil;
        return;
    }
    NSArray *items = [NSJSONSerialization JSONObjectWithData:[contentJSON dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    if (error) {
        NSLog (@"Error loading JSON file %@", error);
        self.recipes = nil;
        return;
    }

    NSMutableArray *recipes = [NSMutableArray new];
    
    for (NSDictionary *item in items) {
    
        NSMutableArray *recipeIngredients = [NSMutableArray new];
        BRRecipe *recipe = [BRRecipe new];
        recipe.name = [item objectForKey:@"name"];
        NSArray *ingredientsArray = [item objectForKey:@"ingredients"];
        
        for (NSDictionary *singleIngredientDict in ingredientsArray) {
            BRIngredient *ingredient = [[BRIngredient alloc] initWithDictionary:singleIngredientDict];
            [recipeIngredients addObject:ingredient];

        }
        recipe.ingredients = recipeIngredients;
        [recipes addObject:recipe];
    }

    self.recipes = recipes;
}

@end

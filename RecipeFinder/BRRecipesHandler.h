//
//  BRRecipesHandler.h
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BRRecipesHandler : NSObject
@property (nonatomic, strong) NSArray *recipes;

- (void)loadRecipesFormFilePath:(NSString*)filePath;

@end

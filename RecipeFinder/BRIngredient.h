//
//  BRIngredient.h
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
typedef enum {
    Of,
    Grams,
    Ml,
    Slices,
    None
} Unit;

#import <Foundation/Foundation.h>

@interface BRIngredient : NSObject
@property (nonatomic, copy) NSString *item;
@property (nonatomic) NSUInteger amount;
@property (nonatomic) Unit unit;
@property (nonatomic, strong) NSDate *useByDate;

- (id)initWithAarray:(NSArray*)array;
- (id)initWithDictionary:(NSDictionary*)dict;

- (NSString *)nameForType:(Unit)unitType;

@end

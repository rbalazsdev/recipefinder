//
//  BRViewController.m
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
#define CSV_FILENAME @"Fridge"
#define JSON_FILENAME @"Recipes"

#import "BRViewController.h"
#import "BRIngredient.h"
#import "BRRecipesHandler.h"
#import "BRRecipe.h"
#import "BRIngredientHandler.h"

@interface BRViewController ()

@property (weak, nonatomic) IBOutlet UILabel *dinnerLabel;
@property (nonatomic, strong) BRIngredientHandler *ingredientsHdlr;
@property (nonatomic, strong) BRRecipesHandler *recipesHdlr;
@property (weak, nonatomic) IBOutlet UITextField *fridgeEntryTextField;
@property (weak, nonatomic) IBOutlet UITextField *recipesEntryTextField;
- (IBAction)findDinnerAction:(id)sender;
@end

@implementation BRViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fridgeEntryTextField.delegate = self;
    self.recipesEntryTextField.delegate = self;
    
    self.ingredientsHdlr = [BRIngredientHandler new];
    self.recipesHdlr = [BRRecipesHandler new];

    NSString *filePathCSV = [[NSBundle mainBundle] pathForResource:CSV_FILENAME ofType:@"csv"];
    self.fridgeEntryTextField.text = filePathCSV;

    NSString *filePathJSON = [[NSBundle mainBundle] pathForResource:JSON_FILENAME ofType:@"json"];
    self.recipesEntryTextField.text = filePathJSON;
  
}

#pragma mark - search for recipe
- (BRRecipe*)recipeForDinner:(NSArray*)recipes fromIngredients:(NSArray*)availableIngredients {

    NSMutableArray *arrayOfRecipes = [NSMutableArray new];
    //find all the recipes that we have ingredients for
    for (BRRecipe* recipe  in recipes) {
        NSUInteger ingredientFound = 0;
        for (BRIngredient *ingredient in recipe.ingredients) {
            if ([self ingredientAvailable:ingredient inFridge:availableIngredients]) {
                ingredientFound++;
            }
        }
        if (ingredientFound == [recipe.ingredients count]) {
            [arrayOfRecipes addObject:recipe];
        }
    }
    //filter the recipes for the expiry date of available ingredients, return recipe with closest doe ingredient
    return [self recipeWithIngredientsWithDueDate:arrayOfRecipes fromIngredients:availableIngredients];
    
}

- (BRRecipe *)recipeWithIngredientsWithDueDate:(NSArray *)recipes fromIngredients:(NSArray *)ingredients {
    //sort the array after expiry dates
    NSArray* sortedAvailableIngredients = [self.ingredientsHdlr sortedArrayOfIngredientsAfterDueDate:ingredients];
    
    //search for expiry date
    for (int i = 0; i < [sortedAvailableIngredients count]; i++) {
        BRIngredient *expiryIngredient = [sortedAvailableIngredients objectAtIndex:i];
        
        //filter the array of recipes
        for (BRRecipe *recipe in recipes) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item like %@", expiryIngredient.item];
            if ([[recipe.ingredients filteredArrayUsingPredicate:predicate] count]>0)
                //recipe found
                return recipe;
        }
    }
    return nil;
}

- (BOOL)ingredientAvailable:(BRIngredient *)ingredient inFridge:(NSArray *)availableIngredients {

    //ingredient name  filtering
    NSPredicate *predAvailable = [NSPredicate predicateWithFormat:@"item like %@", ingredient.item ];
    //amount filtering
    NSPredicate *predQuantity = [NSPredicate predicateWithFormat:@"amount > %d", ingredient.amount ];

    //date filtering
    NSDateComponents *currentComponents = [[NSCalendar currentCalendar]components:NSYearCalendarUnit| NSMonthCalendarUnit| NSDayCalendarUnit fromDate:[NSDate date]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *dateToCheck = [calendar dateFromComponents:currentComponents];
    NSPredicate *predicateExpiryDay = [NSPredicate predicateWithFormat:@"useByDate >= %@", dateToCheck];
    //compound predicate
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predAvailable,predQuantity,predicateExpiryDay]];
    
    NSUInteger index = [availableIngredients indexOfObjectPassingTest:
                          ^(id obj, NSUInteger idx, BOOL *stop) {
                              return [predicate evaluateWithObject:obj];
                          }];
    if (index == NSNotFound) {
        return NO;
    }
    return YES;
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.fridgeEntryTextField) {
        self.fridgeEntryTextField.text = textField.text;
    }
    if (textField == self.recipesEntryTextField) {
        self.recipesEntryTextField.text = textField.text;
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.fridgeEntryTextField) {
        self.fridgeEntryTextField.text = textField.text;
    }
    if (textField == self.recipesEntryTextField) {
        self.recipesEntryTextField.text = textField.text;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)findDinnerAction:(id)sender
{
    [self.fridgeEntryTextField resignFirstResponder];
    [self.recipesEntryTextField resignFirstResponder];
    //load csv file
    NSArray *fridge = [self.ingredientsHdlr loadIngredientsFromResource:self.fridgeEntryTextField.text];
    
    //load json file
    [self.recipesHdlr loadRecipesFormFilePath:self.recipesEntryTextField.text];
    

    //show dinner proposal
    BRRecipe *dinner;
    if ([fridge count] > 0 && [self.recipesHdlr.recipes count]>0) {
        dinner = [self recipeForDinner:self.recipesHdlr.recipes fromIngredients:fridge] ;
    }

    if (!dinner) {
        dinner = [BRRecipe new];
        dinner.name =@"Order Takeout";
        dinner.ingredients = nil;
    }
    
    NSLog(@"Your dinner is : %@",dinner.name);
    self.dinnerLabel.text = dinner.name;
}
@end

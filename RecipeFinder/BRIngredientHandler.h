//
//  BRIngredientHandler.h
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BRIngredientHandler : NSObject
- (NSArray *)loadIngredientsFromResource:(NSString *)fileName;
- (NSArray*)sortedArrayOfIngredientsAfterDueDate:(NSArray*)unsortedArray;

@end

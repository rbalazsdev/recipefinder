//
//  BRIngredientTest.m
//  RecipeFinder
//
//  Created by Razvan Balazs on 7/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BRIngredient.h"

@interface BRIngredientTest : XCTestCase

@end

@implementation BRIngredientTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.

}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testIngredient
{
    BRIngredient *ingredient= [BRIngredient new];
    ingredient.item = @"milk";

    XCTAssertEqualObjects([ingredient item], @"milk");
    XCTAssertNil([ingredient useByDate]);
    
}
- (void)testIngredientFromArray
{
    NSArray *array  = @[@"soup",@"100", @"ml"];
    BRIngredient *ingredient= [[BRIngredient alloc] initWithAarray:array];
    
    XCTAssertEqualObjects([ingredient item], @"soup");
    XCTAssertEqualObjects([NSNumber numberWithInt:[ingredient amount]], [NSNumber numberWithInt: 100]);
    XCTAssertEqualObjects([NSNumber numberWithInt:[ingredient unit]], [NSNumber numberWithInt: 2]);
    XCTAssertNil([ingredient useByDate]);
    
}

- (void)testIngredientFromDict
{
    NSDictionary *dict  = [NSDictionary dictionaryWithObjectsAndKeys:@"soup" ,@"item",
                                                                     @"100" ,@"amount",
                                                                     @"ml" ,@"unit",
                                                                     nil];
    BRIngredient *ingredient= [[BRIngredient alloc] initWithDictionary:dict];
    
    XCTAssertEqualObjects([ingredient item], @"soup");
    XCTAssertEqualObjects([NSNumber numberWithInt:[ingredient amount]], [NSNumber numberWithInt: 100]);
    XCTAssertEqualObjects([NSNumber numberWithInt:[ingredient unit]], [NSNumber numberWithInt: 2]);
    XCTAssertNil([ingredient useByDate]);
    
}

@end

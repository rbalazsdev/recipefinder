//
//  BRIngredient.m
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRIngredient.h"

@implementation BRIngredient

- (id)initWithAarray:(NSArray*)array{
 
    self = [super init];
    if (self) {
        if ([array count]>0) {
            _item = [array firstObject];
        }
        if ([array count] > 1) {
            _amount = [[array objectAtIndex:1] integerValue];
        }
        if ([array count] > 2) {
            NSString *unitType = [array objectAtIndex:2];
            _unit = [self unitFromString:unitType];
        }
        if ([array count]>3) {
            NSDateFormatter* formatter = [NSDateFormatter new];
            [formatter setDateFormat:@"dd/MM/yyyy"];
            _useByDate = [formatter dateFromString:[array objectAtIndex:3]];
        }
    }
    
    return self;
}


- (id)initWithDictionary:(NSDictionary*)dict{
    
    self = [super init];
    if (self) {
        _item = [dict objectForKey:@"item"];
        
        _amount = [[dict objectForKey:@"amount"] integerValue];
        
        NSString *unitType = [dict objectForKey:@"unit"];
        _unit = [self unitFromString:unitType];
        
        //no info for date
        _useByDate = nil;
    }
    
    return self;
}

- (Unit)unitFromString:(NSString *)unitType{
    NSArray *arrayOfUnits = [NSArray arrayWithObjects:@"of",@"grams", @"ml", @"slices",nil];
    Unit unit = None;
    for (NSString *unitString in  arrayOfUnits) {
        if ([unitType isEqualToString:unitString]) {
            unit = [arrayOfUnits indexOfObject:unitString];
            break;
        }
    }
    
    return unit;
}
- (NSArray *)names
{
    static NSMutableArray * _names = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _names = [NSMutableArray arrayWithCapacity:5];
        [_names insertObject:@"of" atIndex:Of];
        [_names insertObject:@"grams" atIndex:Grams];
        [_names insertObject:@"ml" atIndex:Ml];
        [_names insertObject:@"slices" atIndex:Slices];
        [_names insertObject:@"" atIndex:None];
    });
    
    return _names;
}

- (NSString *)nameForType:(Unit)unitType
{
    return [[self names] objectAtIndex:unitType];
}
@end

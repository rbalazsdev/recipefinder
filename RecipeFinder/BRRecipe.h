//
//  BRRecipe.h
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BRRecipe : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSArray *ingredients;
@end

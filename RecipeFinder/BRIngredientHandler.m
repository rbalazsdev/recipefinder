//
//  BRIngredientHandler.m
//  RecipeFinder
//
//  Created by Razvan Balazs on 6/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRIngredientHandler.h"
#import "BRIngredient.h"

@implementation BRIngredientHandler

#pragma  mark - Load ingredients from csv
- (NSArray *)loadIngredientsFromResource:(NSString *)filePath
{
    NSError *error;
    NSString *content =  [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    if (content == nil) {
        NSLog (@"Error loading CSV file %@", error);
    }
    
    NSString *strippedCarriageReturn =[content stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    NSArray *contentArray = [strippedCarriageReturn componentsSeparatedByString:@"\n"];
    NSMutableArray *fridge = [NSMutableArray new];
    
    for (NSString *item in contentArray) {
        NSArray *itemArray = [item componentsSeparatedByString:@","];
        BRIngredient *ingredient = [[BRIngredient alloc] initWithAarray:itemArray];
        [fridge addObject:ingredient];
    }
    return fridge;
}

#pragma mark - helper methods sort
//sort avialable ingredients from dueDate
- (NSArray*)sortedArrayOfIngredientsAfterDueDate:(NSArray*)unsortedArray {
    return  [[NSMutableArray arrayWithArray:unsortedArray ] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *first = [(BRIngredient*)obj1 useByDate];
        NSDate *second = [(BRIngredient*)obj2 useByDate];
        return [first compare:second];
    }];
    
}

@end

//
//  BRRecipeTest.m
//  RecipeFinder
//
//  Created by Razvan Balazs on 7/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BRRecipe.h"

@interface BRRecipeTest : XCTestCase

@end

@implementation BRRecipeTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testRecipe
{
    BRRecipe *recipe = [BRRecipe new];
    recipe.name = @"soup";
    XCTAssertEqualObjects([recipe name], @"soup");
    
}

@end
